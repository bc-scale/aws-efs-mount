1.5.0
====

* maintenance: bumps `efs-utils` to `1.34.1`

1.4.2
====

* fix: checks up to `6` times instead of `3` to mount an EFS, because first mount on AWS can take ages
* fix: check DNS to be ready before attempting to mount

1.4.1
====

* maintenance: pin ubuntu base version to `22.04`
* maintenance: pin EFS utils to `1.33.2`
* fix: makes sure the EFS is writable before sending notify signal
* fix: use docker dind 20+ for builds

1.4.0
====

* feat: Add docker healthcheck
* fix: run efs watchdog manually
* maintenance: pin EFS utils to `1.31.3`
* chore: allow 1 error with dockerlint because of issue with HEALTHCHECK
* chore: remove git protocol for pre-commit hook
* chore: bump pre-commit hook to `4.1.0`

1.3.0
====

* feat: add DNS TXT record support
* chore: ignore mega-lint yaml_vbr plugin

1.2.0
====

* feat: add `SIGRTMIN+3` as standard stop signal (used by `podman` as standard stop signal with systemd)
* refactor: rename entrypoint to match with `podman` systemd standard
* fix: return `fusermount` exit code instead of signal code

1.1.0
====

* feat: Add systemd notification when EFS in ready
* doc: Fix repository URL

1.0.0
=====

* feat: initial version

0.0.0
=====

* tech: initial version
